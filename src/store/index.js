import Vue from 'vue'
import Vuex from 'vuex'
import transcriptions from '@/store/modules/transcriptions' 
Vue.use(Vuex)

export default new Vuex.Store({
  strict: true,
  modules: {
    transcriptions
  }
})
