import transcriptionsAPI from '@/api/transcriptionsAPI'
import * as types from './mutation-types'
const state = {
  currentList: []
}

// getters
const getters = {
  getCurrentList: state => {
    return state.currentList
  }
}

// actions
const actions = {
  async fetchContext({ commit }, { id }) {
    commit(types.FETCH_TRANSCRIPTIONS)
    try {
      const data = await transcriptionsAPI.getAllFromContext({ id })
      commit(types.RECEIVED_TRANSCRIPTIONS, data)
    } catch {
      console.log(`could not fetch transcriptions from ${id}`)
    }
  },
  updateItem({commit}, payload){
    commit(types.UPDATE_TRANSCRIPTION, payload)
  },
  add({commit}){
    commit(types.ADD_TRANSCRIPTION)
  },
  deleteItem({commit}, id) {
    commit(types.REMOVE_TRANSCRIPTION, id)
  },
  async updateContext({commit}, {id, list}){
    // to be used for loading
    // commit(types.REQUEST_UPDATE_TRANSCRIPTIONS)
    console.log(list)
    try {
      const response = await transcriptionsAPI.updateContext({id, list})
      commit(types.RECIEVED_TRANSCRIPTION_UPDATE_ACT, response)
    } catch (error) {
      console.log(error)
    }
  }
}

// mutations
const mutations = {
  [types.FETCH_TRANSCRIPTIONS] (state) {
    state.currentList = undefined
  },
  [types.RECEIVED_TRANSCRIPTIONS] (state,  data ) {
    state.currentList = data
  },
  [types.ADD_TRANSCRIPTION] (state) {
    
    const newTranscription = {
      id: undefined,
      voice: 'Voice',
      text: 'Text'
    }
    newTranscription.id = state.currentList.length + 1
    state.currentList.push(newTranscription)
  },
  [types.REMOVE_TRANSCRIPTION] (state, id ){
    const filteredList = state.currentList.filter( (item) =>  item.id !== id)
    const reorederedList = []
    filteredList.forEach((item, index) => {
      let reorderedItem = item
      reorderedItem.id = index + 1
      reorederedList.push(reorderedItem)
    })
    state.currentList = reorederedList
  },
  [types.REQUEST_UPDATE_TRANSCRIPTIONS] () {
    // console.log('list sent to be updated')
  },
  [types.RECIEVED_TRANSCRIPTION_UPDATE_ACT] (state, payload) {
    state.currentList = payload
  },
  [types.UPDATE_TRANSCRIPTION] (state, payload){
    const index = state.currentList.findIndex(item => item.id === payload.id)
    if (index !== -1) state.currentList.splice(index, 1, payload)
  }
}
export default {
  namespaced: true,
  state,
  getters,
  actions,
  mutations
}