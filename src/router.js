import Vue from 'vue';
import Router from 'vue-router';
// layouts
import DashboardLayout from './layouts/DashboardLayout.vue';
// views
import ListTranscriptions from './views/ListTranscriptions.vue';

Vue.use(Router);

export default new Router({
  mode: 'history',
  base: process.env.BASE_URL,
  routes: [
    {
      path: '',
      component: DashboardLayout,
      children: [
        {
          path: '/',
          redirect: { name: 'ListTranscriptions', params: { id: '5ae1c5792d00004d009d7e5c' } },
        },
        {
          path: 'transcriptions-list/:id',
          name: 'ListTranscriptions',
          component: ListTranscriptions,
          props: (route) => ({
            id: route.params.id
          })
  
        },
      ],
    },
    // {
    //   path: '/about',
    //   name: 'about',
    //   // route level code-splitting
    //   // this generates a separate chunk (about.[hash].js) for this route
    //   // which is lazy-loaded when the route is visited.
    //   component: () => import(/* webpackChunkName: "about" */ './views/About.vue'),
    // },
  ],
});
