import client from '@/api/client'
async function getAllFromContext ({ id }) {
  const response = await client.get(`${id}`)
  return response.data
}
async function updateContext ({ id , payload }){
  const response = await client.post(`${id}`, payload)
  return response.data
}
export default{
  getAllFromContext,
  updateContext
}